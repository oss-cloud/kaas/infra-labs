resource "openstack_networking_network_v2" "management-cluster" {
  name           = "management-cluster"
  admin_state_up = true
  dns_domain     = "kaas.internal."
}

resource "openstack_networking_subnetpool_v2" "management-cluster" {
  name       = "management-cluster"
  ip_version = 4
  prefixes   = ["10.112.0.0/16"]
}

resource "openstack_networking_subnet_v2" "in-cluster" {
  network_id    = openstack_networking_network_v2.management-cluster.id
  subnetpool_id = openstack_networking_subnetpool_v2.management-cluster.id
  cidr          = "10.112.7.0/24"
  name          = "cluster"
  dns_nameservers = [
    "1.1.1.1",
    "1.0.0.1"
  ]
}

resource "openstack_networking_secgroup_v2" "in-cluster" {
  name = "in-cluster"
}

module "secgroup_rule_etcd" {
  source = "./modules/bidirection-secgroup-rule"

  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_group_id   = openstack_networking_secgroup_v2.in-cluster.id
  port_range_min    = 2379
  port_range_max    = 2380
  ethertype         = "IPv4"
  protocol          = "tcp"
}

module "secgroup_rule_kube_apiserver" {
  source = "./modules/bidirection-secgroup-rule"

  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_ip_prefix  = "0.0.0.0/0"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6443
  port_range_max    = 6443
}

module "secgroup_rule_kubelet" {
  source = "./modules/bidirection-secgroup-rule"

  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_group_id   = openstack_networking_secgroup_v2.in-cluster.id
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 10250
  port_range_max    = 10250
}

resource "openstack_networking_secgroup_rule_v2" "allow_node_port" {
  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_ip_prefix  = "0.0.0.0/0"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 30000
  port_range_max    = 32767
}

resource "openstack_networking_secgroup_rule_v2" "allow_http" {
  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_ip_prefix  = "0.0.0.0/0"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
}

resource "openstack_networking_secgroup_rule_v2" "allow_https" {
  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_ip_prefix  = "0.0.0.0/0"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
}

resource "openstack_networking_secgroup_rule_v2" "allow_ssh" {
  security_group_id = openstack_networking_secgroup_v2.in-cluster.id
  remote_ip_prefix  = "0.0.0.0/0"
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
}

resource "openstack_networking_router_v2" "management-cluster" {
  name                = "management-cluster"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.public.id
}

resource "openstack_networking_router_interface_v2" "mangement-cluster" {
  router_id = openstack_networking_router_v2.management-cluster.id
  subnet_id = openstack_networking_subnet_v2.in-cluster.id
}

data "openstack_networking_network_v2" "public" {
  name = "public"
}

