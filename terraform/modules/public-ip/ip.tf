resource "openstack_compute_floatingip_associate_v2" "public_ip" {
  floating_ip = openstack_networking_floatingip_v2.public_ip.address
  instance_id = var.instance_id
}

resource "openstack_networking_floatingip_v2" "public_ip" {
  pool = "public"
}