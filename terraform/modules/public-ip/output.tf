output "address" {
  value = openstack_networking_floatingip_v2.public_ip.address
}

output "dns_name" {
  value = openstack_networking_floatingip_v2.public_ip.dns_name
}

output "dns_domain" {
  value = openstack_networking_floatingip_v2.public_ip.dns_domain
}
