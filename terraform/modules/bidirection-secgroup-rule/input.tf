variable "security_group_id" {
  type = string
}

variable "ethertype" {
  type    = string
  default = "IPv4"
  validation {
    condition     = var.ethertype == "IPv4" || var.ethertype == "IPv6"
    error_message = "Only IPv4 or IPv6 is allowed."
  }
}

variable "protocol" {
  type = string
}

variable "port_range_min" {
  type = number
}

variable "port_range_max" {
  type = number
}

variable "remote_group_id" {
  type     = string
  nullable = true
  default  = null
}

variable "remote_ip_prefix" {
  type     = string
  nullable = true
  default  = null
}
