resource "openstack_networking_secgroup_rule_v2" "ingress_rule" {
  direction         = "ingress"
  ethertype         = var.ethertype
  protocol          = var.protocol
  port_range_min    = var.port_range_min
  port_range_max    = var.port_range_max
  remote_ip_prefix  = var.remote_ip_prefix
  remote_group_id   = var.remote_group_id
  security_group_id = var.security_group_id
}

resource "openstack_networking_secgroup_rule_v2" "egress_rule" {
  direction         = "egress"
  ethertype         = var.ethertype
  protocol          = var.protocol
  port_range_min    = var.port_range_min
  port_range_max    = var.port_range_max
  remote_ip_prefix  = var.remote_ip_prefix
  remote_group_id   = var.remote_group_id
  security_group_id = var.security_group_id
}
