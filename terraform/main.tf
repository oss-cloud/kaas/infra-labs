terraform {
  required_version = ">= 0.14.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/32658518/terraform/state/cluster"
    lock_address   = "https://gitlab.com/api/v4/projects/32658518/terraform/state/cluster/lock"
    lock_method    = "POST"
    unlock_address = "https://gitlab.com/api/v4/projects/32658518/terraform/state/cluster/lock"
    unlock_method  = "DELETE"
  }
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}