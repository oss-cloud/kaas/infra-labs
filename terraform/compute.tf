locals {
  ubuntu_image_id = "a191f164-bdf4-4b1b-90c4-e678e52dcff0"
}

resource "openstack_compute_servergroup_v2" "management-cluster" {
  name     = "management-cluster"
  policies = ["soft-anti-affinity"]
}


data "openstack_compute_keypair_v2" "Macbook" {
  name = "Macbook"
}

resource "openstack_compute_instance_v2" "k8s-node" {
  count = 1

  name        = "k8s-${count.index}"
  image_id    = local.ubuntu_image_id
  flavor_name = "m1.medium"
  key_pair    = data.openstack_compute_keypair_v2.Macbook.name

  network {
    name = openstack_networking_network_v2.management-cluster.name
  }
  security_groups = [
    openstack_networking_secgroup_v2.in-cluster.name
  ]

  scheduler_hints {
    group = openstack_compute_servergroup_v2.management-cluster.id
  }
}

resource "openstack_blockstorage_volume_v3" "etcd-storage" {
  count = length(openstack_compute_instance_v2.k8s-node)

  name                 = "etcd-${count.index}"
  size                 = 10
  enable_online_resize = true
}

resource "openstack_compute_volume_attach_v2" "k8s-etcd" {
  count = length(openstack_compute_instance_v2.k8s-node)

  volume_id   = openstack_blockstorage_volume_v3.etcd-storage[count.index].id
  instance_id = openstack_compute_instance_v2.k8s-node[count.index].id
}

module "k8s-node-public-ip" {
  source = "./modules/public-ip"

  count       = length(openstack_compute_instance_v2.k8s-node)
  instance_id = openstack_compute_instance_v2.k8s-node[count.index].id
}

