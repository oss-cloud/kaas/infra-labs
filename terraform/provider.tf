variable "openstack_project_id" {
  type = string
}

provider "openstack" {
  endpoint_overrides = {
    "compute"  = "https://openstack.cloudnative.tw:8774/v2.1/"
    "dns"      = "https://openstack.cloudnative.tw:9001/"
    "identity" = "https://openstack.cloudnative.tw:5000/"
    "image"    = "https://openstack.cloudnative.tw:9292/"
    "octavia"  = "https://openstack.cloudnative.tw:9876/"
    "volumev3" = "https://openstack.cloudnative.tw:8776/v3/${var.openstack_project_id}/"
  }
}
